package com.nima.digipaytest

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*


class MainViewModel : ViewModel() {

    val result = MutableLiveData<Boolean>()
    val error = MutableLiveData<Int>()

    fun processInput(input: String) {
        if (input.isEmpty()) {
            error.postValue(1)
        } else if (input.length > 1000) {
            error.postValue(2)
        }
        result.postValue(isBalanced(input))
    }

    private fun isMatchingPair(char1: Char, char2: Char): Boolean {
        return if (char1 == '(' && char2 == ')')
            true
        else if (char1 == '{' && char2 == '}')
            true
        else char1 == '[' && char2 == ']'
    }

    private fun isBalanced(exp: String): Boolean {
        val st = Stack<Char>()
        for (i in exp.indices) {
            if (exp[i] == '{' || exp[i] == '(' || exp[i] == '[')
                st.push(exp[i])
            if (exp[i] == '}' || exp[i] == ')' || exp[i] == ']') {
                if (st.isEmpty()) {
                    return false
                } else if (!isMatchingPair(st.pop(), exp[i])) {
                    return false
                }
            }
        }
        return st.isEmpty()
    }
}