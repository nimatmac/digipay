package com.nima.digipaytest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindViewModel()
        setupUI()
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.result.observe(this, Observer { result ->
            if (result) {
                evalBtn?.text = "YES"
            } else {
                evalBtn?.text = "NO"
            }
            Handler().postDelayed({
                evalBtn?.text = "Evaluate"
            }, 3000)
        })
        viewModel.error.observe(this, Observer { errorCode ->
            when (errorCode) {
                1 -> Toast.makeText(this, "input should not be empty", Toast.LENGTH_SHORT).show()
                2 -> Toast.makeText(
                    this,
                    "input should not be more than 1000 characters",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun setupUI() {
        evalBtn?.setOnClickListener {
            val input = inputEt?.text.toString()
            viewModel.processInput(input)
        }
    }


}
